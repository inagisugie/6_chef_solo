Available subcommands: (for details, knife SUB-COMMAND --help)

** OPSCODE PRIVATE CHEF ORGANIZATION MANAGEMENT COMMANDS **
knife opc org create ORG_SHORT_NAME ORG_FULL_NAME (options)
knife opc org delete ORG_NAME
knife opc org edit ORG
knife opc org list
knife opc org show ORGNAME
knife opc org user add ORG_NAME USER_NAME
knife opc org user remove ORG_NAME USER_NAME
knife opc user create USERNAME FIRST_NAME [MIDDLE_NAME] LAST_NAME EMAIL PASSWORD
knife opc user delete USERNAME [-d]
knife opc user edit USERNAME
knife opc user list
knife opc user password USERNAME [PASSWORD | --enable-external-auth]
knife opc user show USERNAME

** BASE COMMANDS **
Usage: /usr/local/bin/knife (options)

** BOOTSTRAP COMMANDS **
knife bootstrap [SSH_USER@]FQDN (options)
knife bootstrap windows ssh FQDN (options)
knife bootstrap windows winrm FQDN (options)

** CLIENT COMMANDS **
knife client bulk delete REGEX (options)
knife client create CLIENTNAME (options)
knife client delete [CLIENT [CLIENT]] (options)
knife client edit CLIENT (options)
knife client key create CLIENT (options)
knife client key delete CLIENT KEYNAME (options)
knife client key edit CLIENT KEYNAME (options)
knife client key list CLIENT (options)
knife client key show CLIENT KEYNAME (options)
knife client list (options)
knife client reregister CLIENT (options)
knife client show CLIENT (options)

** COMMAND COMMANDS **
Usage: /usr/local/bin/knife (options)

** CONFIGURE COMMANDS **
knife configure (options)
knife configure client DIRECTORY

** COOKBOOK COMMANDS **
knife cookbook bulk delete REGEX (options)
Usage: /usr/local/bin/knife (options)
knife cookbook delete COOKBOOK VERSION (options)
knife cookbook download COOKBOOK [VERSION] (options)
knife cookbook list (options)
knife cookbook metadata COOKBOOK (options)
knife cookbook metadata from FILE (options)
knife cookbook show COOKBOOK [VERSION] [PART] [FILENAME] (options)
knife cookbook test [COOKBOOKS...] (options)
knife cookbook upload [COOKBOOKS...] (options)

** COOKBOOK SITE COMMANDS **
knife cookbook site download COOKBOOK [VERSION] (options)
knife cookbook site install COOKBOOK [VERSION] (options)
knife cookbook site list (options)
knife cookbook site search QUERY (options)
knife cookbook site share COOKBOOK [CATEGORY] (options)
knife cookbook site show COOKBOOK [VERSION] (options)
knife cookbook site unshare COOKBOOK

** DATA BAG COMMANDS **
knife data bag create BAG [ITEM] (options)
knife data bag delete BAG [ITEM] (options)
knife data bag edit BAG ITEM (options)
knife data bag from file BAG FILE|FOLDER [FILE|FOLDER..] (options)
knife data bag list (options)
knife data bag show BAG [ITEM] (options)

** EC2 COMMANDS **
knife ec2 ami list (options)
knife ec2 amis ubuntu DISTRO [TYPE] (options)
knife ec2 flavor list (options)
knife ec2 server create (options)
knife ec2 server delete SERVER [SERVER] (options)
knife ec2 server list (options)

** ENVIRONMENT COMMANDS **
knife environment compare [ENVIRONMENT..] (options)
knife environment create ENVIRONMENT (options)
knife environment delete ENVIRONMENT (options)
knife environment edit ENVIRONMENT (options)
knife environment from file FILE [FILE..] (options)
knife environment list (options)
knife environment show ENVIRONMENT (options)

** EXEC COMMANDS **
knife exec [SCRIPT] (options)

** GOOGLE COMMANDS **
knife google disk create NAME --gce-disk-size N (options)
knife google disk delete NAME [NAME] (options)
knife google disk list
knife google project quotas
knife google region list
knife google region quotas
knife google server create NAME -m MACHINE_TYPE -I IMAGE (options)
knife google server delete INSTANCE_NAME [INSTANCE_NAME] (options)
knife google server list
knife google server show INSTANCE_NAME (options)
knife google zone list

** JOB COMMANDS **
knife job list
knife job output <job id> <node> [<node> ...]
knife job start <command> [<node> <node> ...]
knife job status <job id>

** KNIFE COMMANDS **
Usage: /usr/local/bin/knife (options)

** NODE COMMANDS **
knife node bulk delete REGEX (options)
knife node create NODE (options)
knife node delete [NODE [NODE]] (options)
knife node edit NODE (options)
knife node environment set NODE ENVIRONMENT
knife node from file FILE (options)
knife node list (options)
knife node policy set NODE POLICY_GROUP POLICY_NAME (options)
knife node run_list add [NODE] [ENTRY [ENTRY]] (options)
knife node run_list remove [NODE] [ENTRY [ENTRY]] (options)
knife node run_list set NODE ENTRIES (options)
knife node show NODE (options)
knife node status [<node> <node> ...]

** NULL COMMANDS **
knife null

** OSC COMMANDS **
knife osc_user create USER (options)
knife osc_user delete USER (options)
knife osc_user edit USER (options)
knife osc_user list (options)
knife osc_user reregister USER (options)
knife osc_user show USER (options)

** PATH-BASED COMMANDS **
knife delete [PATTERN1 ... PATTERNn]
knife deps PATTERN1 [PATTERNn]
knife diff PATTERNS
knife download PATTERNS
knife edit [PATTERN1 ... PATTERNn]
knife list [-dfR1p] [PATTERN1 ... PATTERNn]
knife show [PATTERN1 ... PATTERNn]
knife upload PATTERNS
knife xargs [COMMAND]

** RAW COMMANDS **
knife raw REQUEST_PATH

** RECIPE COMMANDS **
knife recipe list [PATTERN]

** REHASH COMMANDS **
knife rehash

** RESOURCE COMMANDS **
Usage: /usr/local/bin/knife (options)

** ROLE COMMANDS **
knife role bulk delete REGEX (options)
knife role create ROLE (options)
knife role delete ROLE (options)
knife role edit ROLE (options)
knife role env_run_list add [ROLE] [ENVIRONMENT] [ENTRY [ENTRY]] (options)
knife role env_run_list clear [ROLE] [ENVIRONMENT]
knife role env_run_list remove [ROLE] [ENVIRONMENT] [ENTRIES]
knife role env_run_list replace [ROLE] [ENVIRONMENT] [OLD_ENTRY] [NEW_ENTRY] 
knife role env_run_list set [ROLE] [ENVIRONMENT] [ENTRIES]
knife role from file FILE [FILE..] (options)
knife role list (options)
knife role run_list add [ROLE] [ENTRY [ENTRY]] (options)
knife role run_list clear [ROLE]
knife role run_list remove [ROLE] [ENTRY]
knife role run_list replace [ROLE] [OLD_ENTRY] [NEW_ENTRY] 
knife role run_list set [ROLE] [ENTRIES]
knife role show ROLE (options)

** SEARCH COMMANDS **
knife search INDEX QUERY (options)

** SERVE COMMANDS **
knife serve (options)

** SERVER COMMANDS **
Usage: /usr/local/bin/knife (options)
Usage: /usr/local/bin/knife (options)
Usage: /usr/local/bin/knife (options)
Usage: /usr/local/bin/knife (options)

** SPORK COMMANDS **
knife spork bump COOKBOOK [major|minor|patch|manual]
knife spork check COOKBOOK (options)
knife spork data bag create BAG [ITEM] (options)
knife spork data bag delete BAG [ITEM] (options)
knife spork data bag edit BAG ITEM (options)
knife spork data bag from file BAG FILE|FOLDER [FILE|FOLDER..] (options)
knife spork delete [COOKBOOKS...] (options)
knife spork environment check ENVIRONMENT (options)
knife spork environment create ENVIRONMENT (options)
knife spork environment delete ENVIRONMENT (options)
knife spork environment edit ENVIRONMENT (options)
knife spork environment from file FILENAME (options)
knife spork info
knife spork node create NODE (options)
knife spork node delete NODE (options)
knife spork node edit NODE (options)
knife spork node from file FILE (options)
knife spork node run_list add [NODE] [ENTRY[,ENTRY]] (options)
knife spork node run_list add [NODE] [ENTRY[,ENTRY]] (options)
knife spork node run_list set NODE ENTRIES (options)
knife spork omni COOKBOOK (options)
knife spork promote ENVIRONMENT COOKBOOK (options)
knife spork role create ROLE (options)
knife spork role delete ROLENAME (options)
knife spork role edit ROLENAME (options)
knife spork role from file FILENAME (options)
knife spork upload [COOKBOOKS...] (options)
knife spork version

** SSH COMMANDS **
knife ssh QUERY COMMAND (options)

** SSL COMMANDS **
knife ssl check [URL] (options)
knife ssl fetch [URL] (options)

** STATUS COMMANDS **
knife status QUERY (options)

** SUPERMARKET COMMANDS **
knife supermarket download COOKBOOK [VERSION] (options)
knife supermarket install COOKBOOK [VERSION] (options)
knife supermarket list (options)
knife supermarket search QUERY (options)
knife supermarket share COOKBOOK [CATEGORY] (options)
knife supermarket show COOKBOOK [VERSION] (options)
knife supermarket unshare COOKBOOK (options)

** TAG COMMANDS **
knife tag create NODE TAG ...
knife tag delete NODE TAG ...
knife tag list NODE

** USER COMMANDS **
knife user create USERNAME DISPLAY_NAME FIRST_NAME LAST_NAME EMAIL PASSWORD (options)
knife user delete USER (options)
knife user edit USER (options)
knife user key create USER (options)
knife user key delete USER KEYNAME (options)
knife user key edit USER KEYNAME (options)
knife user key list USER (options)
knife user key show USER KEYNAME (options)
knife user list (options)
knife user reregister USER (options)
knife user show USER (options)

** VAULT COMMANDS **
knife vault create VAULT ITEM VALUES (options)
knife vault delete VAULT ITEM (options)
knife vault download VAULT ITEM PATH (options)
knife vault edit VAULT ITEM (options)
knife vault isvault VAULT ITEM (options)
knife vault itemtype VAULT ITEM (options)
knife vault list (options)
knife vault refresh VAULT ITEM
knife vault remove VAULT ITEM VALUES (options)
knife vault rotate all keys
knife vault rotate keys VAULT ITEM (options)
knife vault show VAULT [ITEM] [VALUES] (options)
knife vault update VAULT ITEM VALUES (options)

** VSPHERE COMMANDS **
knife vsphere cluster list
knife vsphere cpu ratio [CLUSTER] [HOST]
knife vsphere customization list
knife vsphere datastore file
knife vsphere datastore list
knife vsphere datastore maxfree
knife vsphere datastorecluster list
knife vsphere datastorecluster maxfree
knife vsphere folder list
knife vsphere hosts list
knife vsphere pool list
knife vsphere pool query POOLNAME QUERY.  See "http://pubs.vmware.com/vi3/sdk/ReferenceGuide/vim.ComputeResource.html" for allowed QUERY values.
knife vsphere pool show POOLNAME QUERY.  See "http://pubs.vmware.com/vi3/sdk/ReferenceGuide/vim.ComputeResource.html" for allowed QUERY values.
knife vsphere template list
knife vsphere vlan create NAME VID
knife vsphere vlan list
knife vsphere vm cdrom VMNAME (options)
knife vsphere vm clone VMNAME (options)
knife vsphere vm config VMNAME PROPERTY VALUE (PROPERTY VALUE)...
          See "https://www.vmware.com/support/developer/converter-sdk/conv60_apireference/vim.vm.ConfigSpec.html"
          for allowed ATTRIBUTE values (any property of type xs:string is supported).
knife vsphere vm delete VMNAME (options)
knife vsphere vm disk extend VMNAME SIZE. Extends the disk of vm VMNAME to SIZE kilobytes.
knife vsphere vm disk list VMNAME
knife vsphere vm execute VMNAME COMMAND ARGS
knife vsphere vm find
knife vsphere vm list
knife vsphere vm markastemplate VMNAME
knife vsphere vm migrate (options)
knife vsphere vm move
knife vsphere vm net STATE VMNAME
knife vsphere vm network add VMNAME NETWORKNAME
knife vsphere vm network delete VMNAME NICNAME
knife vsphere vm network list VMNAME
knife vsphere vm network set VMNAME NETWORKNAME
knife vsphere vm property get VMNAME PROPERTY.  Gets a vApp Property on VMNAME.
knife vsphere vm property set VMNAME PROPERTY VALUE.  Sets a vApp Property on VMNAME.
knife vsphere vm show VMNAME QUERY.  See "http://pubs.vmware.com/vi3/sdk/ReferenceGuide/vim.VirtualMachine.html" for allowed QUERY values.
knife vsphere vm snapshot VMNAME (options)
knife vsphere vm state VMNAME (options)
knife vsphere vm toolsconfig PROPERTY VALUE.
          See "https://www.vmware.com/support/developer/vc-sdk/visdk25pubs/ReferenceGuide/vim.vm.ToolsConfigInfo.html"
          for available properties and types.
knife vsphere vm vmdk add VMNAME DISK_GB
knife vsphere vm vncset VMNAME COMMAND ARGS
knife vsphere vm wait sysprep VMNAME (options)

** WINDOWS COMMANDS **
knife windows cert generate FILE_PATH (options)
knife windows cert install CERT [CERT] (options)
knife bootstrap windows winrm FQDN (options)
knife bootstrap windows ssh FQDN (options)
knife winrm QUERY COMMAND (options)
knife wsman test QUERY (options)
knife windows listener create (options)

** WINRM COMMANDS **
knife winrm QUERY COMMAND (options)

** WSMAN COMMANDS **
knife wsman test QUERY (options)

